import React from 'react';

import { Card } from '../../components/Card/Card';
import styles from './bestSellers.module.css';

export function BestSellers({ products }) {
  return (
    <article className={styles.contentWrapper}>
      <div className={styles.products}>
        {products.map((item, key) => (
          <Card key={item.productId} product={item} />
        ))}
      </div>
    </article>
  );
}
