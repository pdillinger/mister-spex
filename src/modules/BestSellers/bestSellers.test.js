import { screen, render } from '@testing-library/react';

import { BestSellers } from './BestSellers';


const response = {
  products: [
    {
      tags: ['Antone', 'Dianna', 'Glenna', 'Marisa'],
      title: 'fan heater',
      image: '/images/e0.jpeg',
      productId: '5a9b58fa-5e8e-484f-9071-67e07df6dc17',
      overline: 'Palatine',
      label: 'Granite',
      oldPrice: '377.00',
      price: '315.00',
      subTitle: 'Handmade',
    },
    {
      tags: ['Antone', 'Dianna', 'Glenna', 'Marisa'],
      title: 'thermal mass refrigerator',
      image: '/images/e1.jpeg',
      productId: 'b2b57341-a122-4cd3-aed5-e6d8761e7748',
      overline: 'Johnson City',
      label: 'Plastic',
      oldPrice: '254.00',
      price: '603.00',
      subTitle: 'Fantastic',
    },
    {
      tags: ['Antone', 'Dianna', 'Glenna', 'Marisa'],
      title: 'waffle iron',
      image: '/images/e2.jpeg',
      productId: 'd127581d-9802-47a4-a7b8-5a108b5c8c91',
      overline: 'East Cheyannehaven',
      label: 'Concrete',
      oldPrice: '272.00',
      price: '487.00',
      subTitle: 'Unbranded',
    },
  ],
};

test('render list of products', () => {
  render(<BestSellers products={response.products} />);
  
  expect(screen.getAllByRole('button').length).toEqual(3);
  expect(screen.getAllByRole('figure').length).toEqual(3);
  expect(screen.getAllByRole('img').length).toEqual(3);
  expect(screen.getAllByRole('heading').length).toEqual(6);
  expect(screen.getAllByRole('link').length).toEqual(3);
});

test('render <BestSellers /> module', () => {
  const { asFragment } = render(<BestSellers products={response.products} />);
  expect(asFragment()).toMatchSnapshot();
});
