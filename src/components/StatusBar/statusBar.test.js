import { screen, render } from '@testing-library/react';

import { StatusBar } from './StatusBar';

test('render <StatusBar /> with two tags and one button', () => {
  const tags = ['One', 'Two', 'Three'];
  render(<StatusBar items={tags} />);
  
  expect(screen.queryByText(/one/i)).toBeInTheDocument();
  expect(screen.queryByText(/two/i)).toBeInTheDocument();
  expect(screen.queryByText(/three/i)).not.toBeInTheDocument();
  expect(screen.queryByRole('button', { name: 'add to favorites' })).not.toBeDisabled();
});
