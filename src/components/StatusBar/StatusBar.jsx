import React from 'react';

import { Tags } from '../Tags/Tags';
import { Button } from '../Button/Button';

import styles from './statusBar.module.css';

export function StatusBar({ items }) {
  const tags = items.slice(0, 2);
  return (
    <div className={styles.header}>
      <Tags items={tags} />
      <Button />
    </div>
  );
}
