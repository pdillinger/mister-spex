import React from 'react';

import styles from './tags.module.css';

export function Tags({ items }) {
  return items
    .map((tag) => <span key={tag} className={styles.tag}>{tag}</span>);
}
