import { screen, render } from '@testing-library/react';

import { Tags } from './Tags';

test('render <Tags />', () => {
  const tags = ['One', '+25', 'Top'];
  render(<Tags items={tags} />);

  expect(screen.queryByText(/one/i)).toBeInTheDocument();
  expect(screen.queryByText(/\+25/i)).toBeInTheDocument();
  expect(screen.queryByText(/top/i)).toBeInTheDocument();
});
