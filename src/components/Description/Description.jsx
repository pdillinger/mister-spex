import React from 'react';

import styles from './description.module.css';

export function Description(props) {
  const {label, title, subTitle, oldPrice, price, overline} = props;
  return (
    <div className={styles.wrap}>
      <h2 className={styles.title}>
        <a href="/" className={styles.link}>
          {title}
        </a>
      </h2>
      <h3 className={styles.subtitle}>{subTitle}</h3>
      <p className={styles.overline}>{overline}</p>
      <div className={styles.priceWrap}>
        <span className={styles.label}>{label}</span>
        <div className={styles.priceBox}>
          <div className={styles.oldPrice}>{oldPrice}</div>
          <div className={styles.price}>{price}</div>
        </div>
      </div>
    </div>
  );
}
