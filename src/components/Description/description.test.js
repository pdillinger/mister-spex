import { screen, render } from '@testing-library/react';

import { Description } from './Description';

const props = {
  tags: ['Antone', 'Dianna', 'Glenna', 'Marisa'],
  title: 'trivet',
  image: 'e3.jpeg',
  productId: '7bff7474-badf-4fb7-b269-721596c7798e',
  overline: 'Cristinaville',
  label: 'Concrete',
  oldPrice: '451.00',
  price: '11.00',
  subTitle: 'Practical',
};

test('render <Description /> with product title, link and prices', () => {
  render(<Description {...props} />);

  expect(
    screen.getByRole('heading', { name: props.title })
  ).toBeInTheDocument();
  expect(
    screen.getByRole('heading', { name: props.subTitle })
  ).toBeInTheDocument();
  expect(screen.getByRole('link', { name: props.title })).toBeInTheDocument();

  expect(screen.getByText(/concrete/i)).toBeInTheDocument();
  expect(screen.getByText(/451.00/i)).toBeInTheDocument();
  expect(screen.getByText(/11.00/i)).toBeInTheDocument();
});

test('render <Description /> componet', () => {
  const { asFragment } = render(<Description {...props} />);
  expect(asFragment()).toMatchSnapshot();
});
