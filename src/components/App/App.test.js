import { render } from '@testing-library/react';
import App from './App';

test('renders application', () => {
  const { asFragment } = render(<App />);
  expect(asFragment()).toMatchSnapshot();
});
