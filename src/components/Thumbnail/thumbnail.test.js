import { screen, render } from '@testing-library/react';

import { Thumbnail } from './Thumbnail';

const props = {
  tags: ['Antone', 'Dianna', 'Glenna', 'Marisa'],
  title: 'trivet',
  image: 'e3.jpeg',
  productId: '7bff7474-badf-4fb7-b269-721596c7798e',
  overline: 'Cristinaville',
  label: 'Concrete',
  oldPrice: '451.00',
  price: '11.00',
  subTitle: 'Practical',
};

test('render <Thumbnail /> with correct alt text and description', () => {
  const imageDesc = `${props.title} | ${props.subTitle}`;
  render(
    <Thumbnail
      imageDesc={imageDesc}
      figureText={imageDesc}
      {...props}
    />
  );

  screen.getByRole('figure', { name: "trivet | Practical" });
  screen.getByRole('img', { name: "trivet | Practical" });
});
