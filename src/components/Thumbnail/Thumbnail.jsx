import React from 'react';

import styles from './thumbnail.module.css';

export function Thumbnail({ url, imageDesc, figureText }) {
  return (
    <figure className={styles.thumb} aria-label={figureText}>
      <img src={url} alt={imageDesc} width="638" height="423" loading="lazy" />
    </figure>
  );
}
