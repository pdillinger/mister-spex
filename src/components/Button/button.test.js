import { screen, render } from '@testing-library/react';

import { Button } from './Button';

test('render <Button /> with SVG icon and enabled button', () => {
  render(<Button />);
  const button = screen.getByRole('button', { name: 'add to favorites' });
  expect(button).not.toBeDisabled();
});
