import { screen, render } from '@testing-library/react';

import { Card } from './Card';

const product = {
  tags: ['Antone', 'Dianna', 'Glenna', 'Marisa'],
  title: 'fan heater',
  image: '/images/e0.jpeg',
  productId: '5a9b58fa-5e8e-484f-9071-67e07df6dc17',
  overline: 'Palatine',
  label: 'Granite',
  oldPrice: '377.00',
  price: '315.00',
  subTitle: 'Handmade',
};

test('render <Card /> component with correct elements', () => {
  render(<Card product={product} />);

  screen.getByRole('article', { name: '' });
  screen.getByRole('button', { name: 'add to favorites' });
  screen.getByRole('figure', { name: 'fan heater | Handmade' });
  screen.getByRole('img', { name: 'fan heater | Handmade' });
  screen.getByRole('heading', { name: 'fan heater' });
  screen.getByRole('heading', { name: 'Handmade' });
  screen.getByRole('link', { name: 'fan heater' });
});
