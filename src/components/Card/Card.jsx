import React from 'react';

import { StatusBar } from '../StatusBar/StatusBar';
import { Thumbnail } from '../Thumbnail/Thumbnail';
import { Description } from '../Description/Description';

import styles from './card.module.css';

export function Card({ product }) {
  const { tags, productId, image, title, subTitle, ...rest } = product;
  return (
    <article className={styles.card}>
      <StatusBar items={tags} productId={productId} />
      <Thumbnail
        url={image}
        imageDesc={`${title} | ${subTitle}`}
        figureText={`${title} | ${subTitle}`}
      />
      <Description title={title} subTitle={subTitle} {...rest} />
    </article>
  );
}
