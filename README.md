[![Netlify Status](https://api.netlify.com/api/v1/badges/6b01101d-2480-4753-aee1-86fa68eed0c7/deploy-status)](https://app.netlify.com/sites/gracious-keller-ea0af1/deploys)

# Bestsellers grid (Misterspex)

URL: https://gracious-keller-ea0af1.netlify.app/

![Bestsellers grid](screenshot.png)

#### Folder structure
**Components** - simple react component aka building blocks;

**Modules** - independent, reusable part of the functionality;

**Pages** - page/layout components;

#### CSS
This solution uses CSS modules. Colour and image aspect ratio controlled via CSS variables in the **index.css** file.


#### State management:
No
